<?php
$path = "res/".filter_input(INPUT_GET, 'dir');

session_start();
$_SESSION["test1"] = $path;
$fp = null;

if (file_exists($path)) {
    $fp = fopen($path, 'rb');
} else {
    $fp = fopen('res/default-tile.png', 'rb');
    $path = 'res/default-tile.png';
}

// send the right headers
header("Content-Type: image/png");
header("Content-Length: " . filesize($path));

// dump the picture and stop the script
fpassthru($fp);